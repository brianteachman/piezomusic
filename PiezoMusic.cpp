/*
|| Piezo Tune Player
|| 
|| @package        Piezo Tune Player
|| @author         Brian Teachman <mr.teachman@gmail.com>
|| @url            http://brianteachman@bitbucket.org/brianteachman/piezomusic.git
||
|| @description
|| | This is a hardware abstraction library for piezo elements.
|| | This library provides a simple interface for playing tunes
|| | that are defined as a set of arrays, one containing each notes
|| | frequency and the other containing each notes duration.
|| #
||
|| @license Please see license.txt.
||
*/

#include "PiezoMusic.h" //include the declaration for this class

/**
 * <<constructor>>
 */
PIEZOMUSIC::PIEZOMUSIC(){
    pinMode(melodyPin, OUTPUT);
}

/**
 * <<destructor>>
 */
PIEZOMUSIC::~PIEZOMUSIC(){/*nothing to destruct*/}

/**
 * Play the tune
 * 
 * @param  int[]  melody     Array of song notes
 * @param  int[]  tempo      Array of note tempos
 * @param  int    size       Number of notes in the song
 * @param  float  noteDelay  Multiplier for next note delay
 */
void PIEZOMUSIC::playTune(int melody[], int tempo[], int num_of_bytes, float nextNoteDelay/*=1.30*/) {

	// --------------------------------------------------------------------
	// Why doesn't this work?
	// --------------------------------------------------------------------
	// calculate length
	//int num_of_bytes = sizeof(melody) / sizeof(int);
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	
    for (int thisNote = 0; thisNote < num_of_bytes; thisNote++) {

        // To calculate the note duration, divide one second by the note type.
        // 
        // whole note = 1000 / 1
        // half note  = 1000 / 2
        // quarter note = 1000 / 4
        // eighth note  = 1000 / 8
        // sixteenth note = 1000 / 16
        // thirty-second note = 1000 / 32
        // sixty-fourth note  = 1000 / 64 
        int noteDuration = 1000 / tempo[thisNote];

        this->buzz(melodyPin, melody[thisNote], noteDuration);

        // To distinguish the notes, set a minimum time between them.
        // The note's duration + 30% seems to work well:
        int pauseBetweenNotes = noteDuration * nextNoteDelay;
        delay(pauseBetweenNotes);

        // stop the tone playing:
        this->buzz(melodyPin, 0, noteDuration);
    }
}


/**
 * Ding the buzzer
 * 
 * @param  int[]  melody     Array of song notes
 * @param  int[]  tempo      Array of note tempos
 * @param  int    size       Number of notes in the song
 * @param  float  noteDelay  Multipier for next note delay
 */
void PIEZOMUSIC::buzz(int targetPin, long frequency, long length) {
    long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
    //// 1 second's worth of microseconds, divided by the frequency, then split in half since
    //// there are two phases to each cycle
    long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
    //// multiply frequency, which is really cycles per second, by the number of seconds to
    //// get the total number of cycles to produce
    for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
        digitalWrite(targetPin, HIGH); // write the buzzer pin high to push out the diaphram
        delayMicroseconds(delayValue); // wait for the calculated delay value
        digitalWrite(targetPin, LOW); // write the buzzer pin low to pull back the diaphram
        delayMicroseconds(delayValue); // wait again or the calculated delay value
    }
}

/**
 * Stop dinging the buzzer
 * 
 * @param  int  targetPin  The pin number the piezo is attached to
 */
void PIEZOMUSIC::kill(int targetPin) {
    digitalWrite(targetPin, LOW);
}
