/*
|| Piezo Tune Player
|| 
|| @package        Piezo Tune Player
|| @author         Brian Teachman <mr.teachman@gmail.com>
|| @url            http://brianteachman@bitbucket.org/brianteachman/piezomusic.git
||
|| @description
|| | This is a hardware abstraction library for piezo elements.
|| | This library provides a simple interface for playing tunes
|| | that are defined as a set of arrays, one containing each notes
|| | frequency and the other containing each notes duration.
|| #
||
|| @license Please see license.txt.
||
*/

#ifndef PIEZOMUSIC_H
#define PIEZOMUSIC_H

#define melodyPin 9

// If you are using Arduino 1.0 IDE, change "WProgram.h" to "Arduino.h" 
#include <Arduino.h> // It is very important to remember this! 
#include <Pitches432.h> 
// #include <Pitches440.h> 

class PIEZOMUSIC {
public:
    PIEZOMUSIC();
    ~PIEZOMUSIC();
    void playTune(int notes[], int rhythm[], int num_of_bytes, float nextNoteDelay=1.30);
    void buzz(int targetPin, long frequency, long length);
    void kill(int targetPin);
};

#endif