/*
|| Frequencies for equal-tempered scale, A4 = 432 Hz
|| 
|| @package        Piezo Tune Player
|| @author         Brian Teachman <mr.teachman@gmail.com>
|| @url            http://brianteachman@bitbucket.org/brianteachman/piezomusic.git
|| 
|| Speed of Sound = 345 m/s = 1130 ft/s = 770 miles/hr
|| 
|| @reference http://www.phy.mtu.edu/~suits/notefreq432.html
|| @reference http://www.phy.mtu.edu/~suits/Physicsofmusic.html
*/

#ifndef Pitches432_h
#define Pitches432_h

/*************************************************/
#define NO_SOUND 0

#define NOTE_B0  30
#define NOTE_C1  32
#define NOTE_CS1 34
#define NOTE_D1  36
#define NOTE_DS1 38
#define NOTE_E1  40
#define NOTE_F1  43
#define NOTE_FS1 45
#define NOTE_G1  48
#define NOTE_GS1 51
#define NOTE_A1  54
#define NOTE_AS1 57
#define NOTE_B1  61
#define NOTE_C2  64
#define NOTE_CS2 68
#define NOTE_D2  72
#define NOTE_DS2 76
#define NOTE_E2  81
#define NOTE_F2  86
#define NOTE_FS2 91
#define NOTE_G2  98
#define NOTE_GS2 102
#define NOTE_A2  108
#define NOTE_AS2 114
#define NOTE_B2  121
#define NOTE_C3  128
#define NOTE_CS3 136
#define NOTE_D3  144
#define NOTE_DS3 153
#define NOTE_E3  162
#define NOTE_F3  171
#define NOTE_FS3 182
#define NOTE_G3  192
#define NOTE_GS3 204
#define NOTE_A3  216
#define NOTE_AS3 229
#define NOTE_B3  242
#define NOTE_C4  257
#define NOTE_CS4 272
#define NOTE_D4  288
#define NOTE_DS4 305
#define NOTE_E4  324
#define NOTE_F4  343
#define NOTE_FS4 363
#define NOTE_G4  385
#define NOTE_GS4 408
#define NOTE_A4  432
#define NOTE_AS4 458
#define NOTE_B4  485
#define NOTE_C5  514
#define NOTE_CS5 544
#define NOTE_D5  577
#define NOTE_DS5 611
#define NOTE_E5  647
#define NOTE_F5  686
#define NOTE_FS5 727
#define NOTE_G5  770
#define NOTE_GS5 816
#define NOTE_A5  864
#define NOTE_AS5 915
#define NOTE_B5  970
#define NOTE_C6  1027
#define NOTE_CS6 1089
#define NOTE_D6  1153
#define NOTE_DS6 1222
#define NOTE_E6  1295
#define NOTE_F6  1372
#define NOTE_FS6 1453
#define NOTE_G6  1539
#define NOTE_GS6 1631
#define NOTE_A6  1728
#define NOTE_AS6 1831
#define NOTE_B6  1940
#define NOTE_C7  2055
#define NOTE_CS7 2177
#define NOTE_D7  2307
#define NOTE_DS7 2444
#define NOTE_E7  2589
#define NOTE_F7  2743
#define NOTE_FS7 2906
#define NOTE_G7  3079
#define NOTE_GS7 3262
#define NOTE_A7  3456
#define NOTE_AS7 3662
#define NOTE_B7  3879
#define NOTE_C8  4110
#define NOTE_CS8 4354
#define NOTE_D8  4613
#define NOTE_DS8 4888

#endif