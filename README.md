# Piezo Music Player #

### What is this? ###

This library is a hardware abstraction library for piezo elements and provides a simple interface for playing tunes
that are defined as a set of arrays, one containing each notes frequency and the other containing each notes duration.

### How do I get set up? ###

1. Just add this to whereever the libraries folder that your IDE uses.
2. Added #include <PiezoMusic.h> to the top of your sketch.
3. Create new player: PIEZOMUSIC player;
4. Play song: player.playTune(melody, tempo, noteCount, nextNoteDelay);