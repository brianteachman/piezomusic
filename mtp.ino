#include <PiezoMusic.h>

const int playButtonPin = 2;
const int selectorButtonPin = 3;
const int ledPin = 8;
const int piezoPin = 9;

/* -- Button state -----------------------------------------------------------*/

const int PLAYBUTTON = 1;
const int SELECTORBUTTON = 2;

int playButtonPushCounter = 0;   // counter for the number of button presses
int playButtonState = 0;         // current state of the button
int playLastButtonState = 0;     // previous state of the button

int selectorButtonPushCounter = 0;   // counter for the number of button presses
int selectorButtonState = 0;         // current state of the button
int selectorLastButtonState = 0;     // previous state of the button


/* -- Initialize the piezo driver --------------------------------------------*/
PIEZOMUSIC player;


/* -- The songs --------------------------------------------------------------*/

char* songNames[] = {"Mario Bros Theme", "Mario Bros Underworld", "James Bond Theme", "Peer Gynt"};

/**
 * Mario Bros: Main theme melody
 * 
 * songNames[0]
 */
 int marioBrosMainMelody[] = {NOTE_E7,NOTE_E7,NO_SOUND,NOTE_E7,NO_SOUND,NOTE_C7,NOTE_E7,NO_SOUND,NOTE_G7,NO_SOUND,NO_SOUND,NO_SOUND,NOTE_G6,NO_SOUND,NO_SOUND,NO_SOUND,NOTE_C7,NO_SOUND,NO_SOUND,NOTE_G6,NO_SOUND,NO_SOUND,NOTE_E6,NO_SOUND,NO_SOUND,NOTE_A6,NO_SOUND,NOTE_B6,NO_SOUND,NOTE_AS6,NOTE_A6,NO_SOUND,NOTE_G6,NOTE_E7,NOTE_G7,NOTE_A7,NO_SOUND,NOTE_F7,NOTE_G7,NO_SOUND,NOTE_E7,NO_SOUND,NOTE_C7,NOTE_D7,NOTE_B6,NO_SOUND,NO_SOUND,NOTE_C7,NO_SOUND,NO_SOUND,NOTE_G6,NO_SOUND, NO_SOUND, NOTE_E6, NO_SOUND, NO_SOUND, NOTE_A6, NO_SOUND, NOTE_B6, NO_SOUND, NOTE_AS6, NOTE_A6, NO_SOUND, NOTE_G6, NOTE_E7, NOTE_G7, NOTE_A7, NO_SOUND, NOTE_F7, NOTE_G7, NO_SOUND, NOTE_E7, NO_SOUND, NOTE_C7, NOTE_D7, NOTE_B6, NO_SOUND ,NO_SOUND};
 int marioBrosMainTempo[] = {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 9, 9, 9, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 9, 9, 9, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

/**
 * Mario Bros: Underworld melody
 * 
 * songNames[1]
 */
 int marioBrosUnderworldMelody[] = {NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_AS3, NOTE_AS4, NO_SOUND, NO_SOUND, NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_AS3, NOTE_AS4, NO_SOUND, NO_SOUND, NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4, NOTE_DS3, NOTE_DS4, NO_SOUND, NO_SOUND, NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4, NOTE_DS3, NOTE_DS4, NO_SOUND, NO_SOUND, NOTE_DS4, NOTE_CS4, NOTE_D4, NOTE_CS4, NOTE_DS4, NOTE_DS4, NOTE_GS3, NOTE_G3, NOTE_CS4, NOTE_C4, NOTE_FS4, NOTE_F4, NOTE_E3, NOTE_AS4, NOTE_A4, NOTE_GS4, NOTE_DS4, NOTE_B3, NOTE_AS3, NOTE_A3, NOTE_GS3, NO_SOUND, NO_SOUND, NO_SOUND};
 int marioBrosUnderworldTempo[] = {12, 12, 12, 12, 12, 12, 6, 3, 12, 12, 12, 12, 12, 12, 6, 3, 12, 12, 12, 12, 12, 12, 6, 3, 12, 12, 12, 12, 12, 12, 6, 6, 18, 18, 18, 6, 6, 6, 6, 6, 6, 18, 18, 18, 18, 18, 18, 10, 10, 10, 10, 10, 10, 3, 3, 3};

/**
 * James Bond Theme song
 */
 int JamesBondMelody[] = {NOTE_E4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_E4,NOTE_E4,NOTE_E4, NOTE_E4,NOTE_G4,NOTE_G4,NOTE_G4,NOTE_G4,NOTE_E4,NOTE_E4,NOTE_E4, NOTE_E4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_E4,NOTE_E4,NOTE_E4, NOTE_E4,NOTE_G4,NOTE_G4,NOTE_G4,NOTE_G4,NOTE_E4,NOTE_E4,NOTE_E4, NOTE_DS5,NOTE_D5,NOTE_B4,NOTE_A4,NOTE_B4, NOTE_E4,NOTE_G4,NOTE_DS5,NOTE_D5,NOTE_G4,NOTE_B4, NOTE_B4,NOTE_FS5,NOTE_F5,NOTE_B4,NOTE_D5,NOTE_AS5, NOTE_A5,NOTE_F5,NOTE_A5,NOTE_DS6,NOTE_D6};
 int JamesBondTempo[] = {8,16,16,8,4,8,8,8, 8,16,16,8,4,8,8,8, 8,16,16,8,4,8,8,8, 8,16,16,8,4,8,8,8, 8,2,8,8,1, 8,4,8,4,8,8, 8,8,4,8,4,8, 4,8,4,8,3};

/**
 * Peer Gynt Suite No.1, by Edvard Grieg
 */
int peergynt_m[] = {NOTE_G4, NOTE_E4, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_E4, NOTE_G4, NOTE_E4, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_E4, NOTE_D4, NOTE_E4, NOTE_G4, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_E4, NOTE_A4, NOTE_G4, NOTE_E4, NOTE_D4, NOTE_C4};
int peergynt_r[] = {8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 16};

/**
 * ?
 */
//int natal_m[] = {G, A, G, E, G, A, G, E, c, c, A, B, B, G, A, G, A, c, B, A, G, A, G, E};
//int natal_r[] = {12, 4, 8, 16, 12, 4, 8, 16, 12, 4, 16, 12, 4, 16, 12, 4, 8, 8, 8, 8, 12, 4, 8, 16};

/**
 * ?
 */
//int LTS_m[] = {Bb, G, G, Bb, G, G, Bb, G, G, Bb, G, G, Bb, G, C, G, Bb, G, G, Bb, G, G, Bb, G, G, Bb, G, G, Bb, G, F, D, F, D, G, F, D, C, Bb, G, Bb, C, C1, C, Bb, F, D, Bb, G, F, D, C, Bb, D, C, Bb, G} ;
//int LTS_r[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};


/* -- Helpers functions ------------------------------------------------------*/

// Returns true if the state of the button has changed
bool stateChanged(int component)
{
  switch (component) {
    case PLAYBUTTON:
      return playButtonState != playLastButtonState;
      break;
    case SELECTORBUTTON:
      return selectorButtonState != selectorLastButtonState;
      break;
    default:
      Serial.print("Component #");
      Serial.print(component);
      Serial.println(" doesn't exist");
      return false;
  }
}

// Returns true if the button is in "ON" and increments the button counter
 bool isPlaying(int buttonPushCounter)
 {
  playButtonPushCounter++;
  return (buttonPushCounter % 2 == 0) ? false: true;
}

// Uses modulo to limit the domain of the number of button pushs value = [0, 3]
// Which cycles through four song selections
int getSongIndex()
{
  return selectorButtonPushCounter % 4;
}

// Start playing tune and print meta information over serial
void play(int songIndex)
{
  Serial.print( "Playing: " );
  Serial.println( songNames[songIndex] );

  int noteCount;
  switch (songIndex) {
    case 0: {
      noteCount = sizeof(marioBrosMainMelody) / sizeof(int);
      player.playTune(marioBrosMainMelody, marioBrosMainTempo, noteCount);
      break;
    }
    case 1: {
      noteCount = sizeof(marioBrosUnderworldMelody) / sizeof(int);
      player.playTune(marioBrosUnderworldMelody, marioBrosUnderworldTempo, noteCount);
      break;
    }
    case 2: {
      noteCount = sizeof(JamesBondMelody) / sizeof(int);
      player.playTune(JamesBondMelody, JamesBondTempo, noteCount, 1);
      break;
    }
    case 3: {
      noteCount = sizeof(peergynt_m) / sizeof(int);
      player.playTune(peergynt_m, peergynt_r, noteCount, 1.5);
      break;
    }
    default:
      Serial.println("Somethings wrong here!");
  }
  
  // Turn off the LED
  digitalWrite(ledPin, LOW);
  
  Serial.print("Finished playing ");
  Serial.print(noteCount);
  Serial.println(" notes");
}


/* -- MAIN              ------------------------------------------------------*/

void setup() {
  pinMode(playButtonPin, INPUT);
  pinMode(selectorButtonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(piezoPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {

  // Read the pushbutton input pins ( HIGH / LOW )
  playButtonState = digitalRead(playButtonPin);
  selectorButtonState = digitalRead(selectorButtonPin);

  // If the Selector Button is pressed, change the song
  if ( stateChanged(SELECTORBUTTON) ) {

    // If the button is pressed, increment the counter
    if (selectorButtonState == HIGH) {
      selectorButtonPushCounter++;
      
      Serial.print("Loaded: ");
      Serial.println( songNames[getSongIndex()] );
    }
  }
  // We'll compare these two vars next time through the loop 
  selectorLastButtonState = selectorButtonState;

  // If the Play Button is pressed when a song is not playing, then:
  if ( stateChanged(PLAYBUTTON) ) {
    if ( isPlaying(playButtonPushCounter) ) {

      // Turn on the LED
      digitalWrite(ledPin, HIGH);
      
      // Play the song at index
      play( getSongIndex() );
    }
  }
  // We'll compare these next time through the loop 
  playLastButtonState = playButtonState;
}
